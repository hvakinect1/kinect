
library("dplyr")

# This script assumes you have loaded kinect tests in the 'tests' list

# Each of the points for which the ranges will be calculated
coordinateNames <- c("SB", "SM", "N", "H", "SL", "EL", "WL", "HL", "SR", "ER", "WR", "HR", "HiL", "KN", "AL", "FL", "HiR", "KR", "AR", "FR", "SS", "HTL", "TL", "HTR", "TR")
# Each of the dimensions for which the ranges will be calculated
coordinateDimensions <- c("x", "y", "z")

calculatedTests = list()
total <- length(tests)
for (i in 1:total) {
  areaDf <- tests[[i]]
  for (coordinateName in coordinateNames) {
    for (dimension in coordinateDimensions) {
      trackedDf <- areaDf %>% filter(areaDf[[paste(coordinateName, "t", sep = "")]] == 2)
      fieldName <- paste(coordinateName, dimension, sep = "")
      min <- min(trackedDf[[fieldName]])
      max <- max(trackedDf[[fieldName]])
      if (!(is.infinite(min) || is.infinite(max))) {
        mlr <- abs(min - max)
        areaDf[[paste(fieldName, "r", sep = "")]] <- mlr
      }
      else {
        areaDf[[paste(fieldName, "r", sep = "")]] <- NA
        print(paste(trackedDf[1,1], fieldName, min, max, sep = " - "))
      }
    }
  }
  print(paste(i, "/", total))
  calculatedTests[[i]] <- areaDf
}

# Connect to MongoDB
con <- mongo(collection = "lowlands", db = "kinect", url = "mongodb://localhost",
             verbose = FALSE, options = ssl_options())

# Drop collection and insert new collection 
con$drop()
for (i in 1:length(calculatedTests)) {
  df <- calculatedTests[[i]]
  con$insert(df)
  print(i)
}
