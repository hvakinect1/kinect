
function.multiplyCharacterVectors <- function(leftHandVector, rightHandVector) {
  return(
    gsub(" ", "", x = sort(apply(expand.grid(leftHandVector, rightHandVector), 1, paste, collapse = "", seperator = "")))
  )
}

function.getPersonalFilters <- function(gender = NULL, heightRange = NULL, weightRange = NULL) {
  match_gender <- ifelse(test = !is.null(gender), yes = paste('{ "Gender": ', gender, ' }', sep = ""), no = "")
  match_height <- ifelse(test = !is.null(heightRange), yes = paste('{ "Height": { "$gt": ', heightRange[1], ', "$lt": ', heightRange[2], ' } }'), no = "")
  match_weight <- ifelse(test = !is.null(weightRange), yes = paste('{ "Weight": { "$gt": ', weightRange[1], ', "$lt": ', weightRange[2], ' } }'), no = "")
  
  match_conditions <- c()
  if (nchar(match_gender) > 0) { match_conditions[length(match_conditions) + 1] <- match_gender }
  if (nchar(match_height) > 0) { match_conditions[length(match_conditions) + 1] <- match_height }
  if (nchar(match_weight) > 0) { match_conditions[length(match_conditions) + 1] <- match_weight }
  return(match_conditions)
}

