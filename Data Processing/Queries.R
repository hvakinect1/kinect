# Mongo connection
library(mongolite)

db <- mongo(collection = "lowlands", db = "kinect", url = "mongodb://localhost",
            verbose = FALSE, options = ssl_options())

db.pathdistances <- mongo(collection = "balans_distance", db = "kinect", url = "mongodb://localhost",
                       verbose = FALSE, options = ssl_options())

db.pathdistancesWalk <- mongo(collection = "walk_distance", db = "kinect", url = "mongodb://localhost",
                           verbose = FALSE, options = ssl_options())

walk.db <- mongo(collection = "looptest", db = "kinect", url = "mongodb://localhost",
                 verbose = FALSE, options = ssl_options())

db.walk_speed <- mongo(collection = "walk_speed", db = "kinect", url = "mongodb://localhost",
                 verbose = FALSE, options = ssl_options())

# Query file 
query.withGogglesBalanceTest <- db$aggregate('[{"$match" : { "Goggles": 1}},{"$group": {"_id": "$tID"}},{"$count": "With Goggles"}]')
query.withGogglesLoopTest <- walk.db$aggregate('[{"$match" : { "Goggles": 1}},{"$group": {"_id": "$tID"}},{"$count": "With Goggles"}]')
query.withGoggles <- query.withGogglesBalanceTest + query.withGogglesLoopTest
query.amountWomanTests <- db$aggregate('[{"$match" : { "Gender": 1}},{"$group": {"_id": "$sID"}},{"$count": "Amount woman"}]')
query.avgAge <- db$aggregate('[{ "$group" : { "_id": "null", "Age": { "$avg": "$Age" } } } ]')$Age
query.heightAmount <-  db$aggregate('[
    { "$group" : { "_id" : {"sID": "$sID", "Height": "$Height"}}},
    { "$group" : { "_id": "$_id.Height","Total" : { "$sum" : 1 }}},
    { "$project": { "_id": 0, "Height": "$_id", "Total": 1 }},
    { "$sort": { "Height" : 1}}
  ]')
query.avgLengthWoman <- db$aggregate('[
    { "$match" : { "Gender" : 1}},
    { "$group" : { "_id": "null", "AVG_Height": { "$avg": "$Height" }}}
]')

query.avgLengthMan <- db$aggregate('[
    { "$match" : { "Gender" : 0}},
    { "$group" : { "_id": "null", "AVG_Height": { "$avg": "$Height" }}}
]')
query.pathDistancesWalk_TotalManhattanDistance <- db.pathdistancesWalk$aggregate('[
{ "$group": { "_id": "null", 
  "distXGait1": { "$sum": "$HxN1" }, 
  "distXGait2": { "$sum": "$HxN2" }, 
  "distYGait1": { "$sum": "$HyN1" }, 
  "distYGait2": { "$sum": "$HyN2" } 
  } },
  { "$project": { "distance": { "$add": [ "$distXGait1", "$distXGait2", "$distYGait1", "$distYGait2" ] } } } 
  ]')


query.avgSpeed <- db.walk_speed$aggregate('[
     { "$group": { "_id": "null", "avgSpeed": { "$avg": { "$divide": [ { "$sum": [ "$speedG", "$speedN" ] }, 2 ] } } } }
]')

query.pointdiff <- function(point1, point2, gender = NULL, heightRange = c(0, 300), weightRange = c(0, 300)) {
  
  match_conditions <- function.getPersonalFilters(gender = gender, heightRange = heightRange, weightRange = weightRange)
  match_ands <- paste(match_conditions, sep = "", collapse = ", ")
  
  match_stage <- ifelse(test = length(match_ands) > 0,
                        yes = paste('{ "$match": { "$and": [ ', paste(match_ands, sep = ", "), ' ] } },'),
                        no = "")
  
  data <- db$aggregate(paste('[ ', match_stage, '
                            { "$group": { "_id": "$tID", ', 
                            '"x": { "$avg": { "$sum": { "$abs": { "$subtract": [ "$',
                              point1, 'x", "$',
                              point2, 'x" ] } } } },', 
                            '"y": { "$avg": { "$sum": { "$abs": { "$subtract": [ "$',
                             point1, 'y", "$',
                             point2, 'y" ] } } } },', 
                            '"z": { "$avg": { "$sum": { "$abs": { "$subtract": [ "$',
                             point1, 'z", "$',
                             point2, 'z" ] } } } }',
                     ' } } ]', sep = "")
               )
  
  colnames(data)[1] <- "id"
  
  ids <- substr(data$id, 0, nchar(data$id) - 1)
  ids <- ids[duplicated(ids)]
  
  df <- data.frame(matrix(ncol = 7))
  colnames(df) <- c("id", "xN", "xG", "yN", "yG", "zN", "zG")
  
  for(id in ids) {
    newRow <- data.frame(id = id,
              xN = data[data$id == paste(id, "0", sep = ""),]$x,
              xG = data[data$id == paste(id, "1", sep = ""),]$x,
              yN = data[data$id == paste(id, "0", sep = ""),]$y,
              yG = data[data$id == paste(id, "1", sep = ""),]$y,
              zN = data[data$id == paste(id, "0", sep = ""),]$z,
              zG = data[data$id == paste(id, "1", sep = ""),]$z
    )

    df <- rbind(df, newRow)
  }
  
  return(na.omit(df))
}

# Calculate the avarage defference between two points in a single test
query.pointdiffWalk <- function(point1, point2, gender = NULL, heightRange = c(0, 300), weightRange = c(0, 300)) {
  
  # Apply filters
  match_conditions <- function.getPersonalFilters(gender = gender, heightRange = heightRange, weightRange = weightRange)
  match_ands <- paste(match_conditions, sep = "", collapse = ", ")
  # Create filter query
  match_stage <- ifelse(test = length(match_ands) > 0,
                        yes = paste('{ "$match": { "$and": [ ', paste(match_ands, sep = ", "), ' ] } },'),
                        no = "")
  # Excecute mongo DB query to calculate the difference
  data <- walk.db$aggregate(paste('[ ', match_stage, '
                            { "$group": { "_id": "$tID", ', 
                            '"x": { "$avg": { "$sum": { "$abs": { "$subtract": [ "$',
                              point1, 'x", "$',
                              point2, 'x" ] } } } },', 
                            '"y": { "$avg": { "$sum": { "$abs": { "$subtract": [ "$',
                             point1, 'y", "$',
                             point2, 'y" ] } } } },', 
                            '"z": { "$avg": { "$sum": { "$abs": { "$subtract": [ "$',
                             point1, 'z", "$',
                             point2, 'z" ] } } } }',
                     ' } } ]', sep = "")
               )
  
  # Only keep test who did a test with AND without goggles
  colnames(data)[1] <- "id"
  ids <- substr(data$id, 0, nchar(data$id) - 2)
  ids <- ids[duplicated(ids)]
  
  df <- data.frame(matrix(ncol = 7))
  colnames(df) <- c("id", "xN", "xG", "yN", "yG", "zN", "zG")
  
  # To make the plot relevant, only keep tests with two gaits
  for(id in ids) {
      xN <- data[data$id == paste(id, "01", sep = ""),]
      xG <- data[data$id == paste(id, "11", sep = ""),]
      yN <- data[data$id == paste(id, "01", sep = ""),]
      yG <- data[data$id == paste(id, "11", sep = ""),]
      zN <- data[data$id == paste(id, "01", sep = ""),]
      zG <- data[data$id == paste(id, "11", sep = ""),]
      
      if(nrow(xN) > 0 && nrow(xG) > 0) {
        newRow <- data.frame(id = id,
              xN = xN$x,
              xG = xG$x,
              yN = yN$y,
              yG = yG$y,
              zN = zN$z,
              zG = zG$z
        )
    
        df <- rbind(df, newRow)
      }
  }
  
  return(na.omit(df))
}

query.pathDistances <- function(gender = NULL, heightRange = c(0, 300), weightRange = c(0, 300)) {
  match_conditions <- function.getPersonalFilters(gender, heightRange, weightRange)
  find_ands <- ifelse(test = length(match_conditions) > 0,
                      yes = paste(match_conditions, sep = "", collapse = ", "),
                      no = "")
  if (nchar(find_ands) > 0) {
    return(db.pathdistances$find())
    # TODO: the balans_distance collection has no reference to personal data (gender, height, weight)
    #return(db.pathdistances$find(paste('{ "$and": [ ', find_ands , ' ] }', sep = "", collapse = ", ")))
  }
  else {
    return(db.pathdistances$find())
  }
}

query.pathDistancesWalk <- function(gender = NULL, heightRange = c(0, 300), weightRange = c(0, 300)) {
  match_conditions <- function.getPersonalFilters(gender, heightRange, weightRange)
  find_ands <- ifelse(test = length(match_conditions) > 0,
                      yes = paste(match_conditions, sep = "", collapse = ", "),
                      no = "")
  if (nchar(find_ands) > 0) {
    return(db.pathdistancesWalk$find(paste('{ "$and": [ ', find_ands , ' ] }', sep = "", collapse = ", ")))
  }
  else {
    return(db.pathdistancesWalk$find())
  }
}


query.testsWithAndWithoutGoggles <- function() {
  testIDs <- substr(db$distinct("tID"), 0, nchar(db$distinct("tID")) - 1)
  testIDs <- testIDs[duplicated(testIDs)]
  return(testIDs)
}

query.walkTestsWithAndWithoutGoggles <- function() {
  testIDs <- substr(walk.db$distinct("tID"), 0, nchar(walk.db$distinct("tID")) - 2)
  testIDs <- testIDs[duplicated(testIDs)]
  return(testIDs)
}

query.areaCalculation <- function(testIDs, coordinates, dimensions = c("x", "y", "z"), printQuery = FALSE, gender = NULL, heightRange = c(0, 300), weightRange = c(0, 300)) {
  #Determine all test to be retrieved
  fields <- function.multiplyCharacterVectors(coordinates, dimensions)
  in_IDs <- paste(testIDs, collapse = "\", \"")
  match_ids <- paste('{ "tID": { "$in": [ "', in_IDs, '" ] } }', sep = "")
  
  #Create all conditions for a match-and stage
  match_conditions <- function.getPersonalFilters(gender = gender, heightRange = heightRange, weightRange = weightRange)
  if (nchar(match_ids) > 0) { match_conditions[length(match_conditions) + 1] <- match_ids }
  
  #Determine aggregate stages
  match_ands <- paste(match_conditions, sep = "", collapse = ", ")
  group_stage <- paste(paste('"', fields, '_max" : { "$max" : "$', fields, '" }, "', fields, '_min" : { "$min" : "$', fields, '" }', sep = ""), collapse = ", ")
  project_stage <- paste(paste('"', fields, '_r" : { "$subtract" : ["$', fields,'_max", "$', fields, '_min"] }', sep = ""), collapse = ", ")
  
  aggregate_query <- paste('[
     { "$match": { "$and": [ ', paste(match_ands, sep = ", "), ' ] } },
     { "$group": { "_id": "$tID", ', group_stage, ' } },
     { "$project": { "_id" : "$_id", ', project_stage, ' } } ]', sep = "")
  if (printQuery) {
    print(aggregate_query)
  }
  return(db$aggregate(aggregate_query))
}

query.walkAreaCalculation <- function(testIDs, coordinates, dimensions = c("x", "y", "z"), printQuery = FALSE, gender = NULL, heightRange = c(0, 300), weightRange = c(0, 300)) {
  #Determine all test to be retrieved
  fields <- function.multiplyCharacterVectors(coordinates, dimensions)
  in_IDs <- paste(testIDs, collapse = "\", \"")
  match_ids <- paste('{ "tID": { "$in": [ "', in_IDs, '" ] } }', sep = "")
  
  #Create all conditions for a match-and stage
  match_conditions <- function.getPersonalFilters(gender = gender, heightRange = heightRange, weightRange = weightRange)
  if (nchar(match_ids) > 0) { match_conditions[length(match_conditions) + 1] <- match_ids }
  
  #Determine aggregate stages
  match_ands <- paste(match_conditions, sep = "", collapse = ", ")
  group_stage <- paste(paste('"', fields, '_max" : { "$max" : "$', fields, '" }, "', fields, '_min" : { "$min" : "$', fields, '" }', sep = ""), collapse = ", ")
  project_stage <- paste(paste('"', fields, '_r" : { "$subtract" : ["$', fields,'_max", "$', fields, '_min"] }', sep = ""), collapse = ", ")
  
  aggregate_query <- paste('[
     { "$match": { "$and": [ ', paste(match_ands, sep = ", "), ' ] } },
     { "$group": { "_id": "$tID", ', group_stage, ' } },
     { "$project": { "_id" : "$_id", ', project_stage, ' } } ]', sep = "")
  if (printQuery) {
    print(aggregate_query)
  }
  df <- walk.db$aggregate(aggregate_query)
  
  return(walk.db$aggregate(aggregate_query))
}
