
## Data Cleaning

De data bestaan in principe uit twee onderdelen. Namelijk de informatie over de testpersonen, en de Kinect data. Voordat alle data verwerkt kan worden in een database zal het eerst opgeschoond en gekoppeld moeten worden. 

### Subject Data

In subject data zijn de volgende velden verwijderd:
 
```
G1_bril, G1_norm, G2_bril, G2_norm, G3_bril, G3_norm, 
S1_bril, S1_norm, S2_bril, S2_norm, S3_bril, S3_norm, 
B1_bril, B1_norm, B2_bril, B2_norm, B3_bril, B3_norm, 
Goggles, NumberOfTests, 
```

De waardes met en zonder bril (G1_bril etc.) kunnen uiteindelijk worden gevonden in de data, dit hoeft per gebruiker niet nogmaals opgeslagen te worden. Dit zal een combinatie worden van de velden *TestSoort*, *goggles* en *TestNummber*. De *NumberOfTests* kan gemeten worden hoe vaak een gebruiker voor komt in de database. 

### Test Info Kinect

In de data van *TestInfoKinect* was de *DateTime* in zes verschillende velden opgeslagen (namelijk year, month, day, hour, minute en second). Dit is gebuneld naar 1 column genaamd *datetime*. 

In dit bestand zat ook TestInfo van de zogeheten 'gait' test. Echter hebben wij alleen de kinect data van de `balans` test. Om deze reden hebben we alle gegevens van de gait test verwijderd; 

```r 
testinfokinect <- testinfokinect[!testinfokinect$Test == "gait", ]
```

Daarnaast kwamen we er achter dat er duplicate data in zat (2 testen op exact dezelfde timestamp is niet mogelijk). Deze hebben we eruitghaald door middel van de `duplicated` functie, namelijk: 

``` r
testinfokinect <- testinfokinect[!duplicated(testinfokinect), ]
```

### Alcohol Scores 

In het bestand van Alcohol Scores zijn we iets merkwaardigs tegengekomen, er is namelijk een subject geweest die wel een blaastest heeft afgenomen maar geen van de testen met de kinect (subject komt niet voor in testInfoKinect). Door middel van de `filter` functie uit de *dplyr* package is deze eruit gehaald om incorrecte data te voorkomen. 

``` r
alcoholscores <- alcoholscores %>% filter(!(SubjectID == "29" & time == "13:50"))
```

## Data samenvoegen

Nadat alle drie de datafiles zijn ingeladen en schoongemaakt met `R` kan de data samengevoegd worden. Alle drie de dataframes bevatten een `subjectID`, deze ID kan gebruikt worden om de dataframes te mergen. 

Als eerste wordt de `subjectinfo` met `testinfokinect` gemerged. 

``` r
df <- merge(subjectinfo, testinfokinect, by = "SubjectID")
```

Het nieuwe gemergde dataframe heet `df`. In dit nieuwe dataframe worden de alcohol testen toegevoegd. Deze wordt op twee 'keys' gemerged, namelijk `SubjectID` en `TestNr`. Dit is namelijk omdat een SubjectID niet uniek is. Sommige personen hebben vaker de test afgelegd en hebben daarbij ook vaker de alcohol test gedaan. Om deze reden wordt `TestNr` hierbij toegevoegd. Maar zelfs `SubjectID` en `TestNr` samen is nog niet uniek. Deze komen bijna altijd twee maal voor. Bijna iedere subject heeft namelijk dezelfde test twee keer gedaan, met en zonder simulatie bril. Dit wordt gemeten als 1 test (dus feitelijk heeft een subject met 1 alcoholtest 2 test uitvoeringen). Daarom is het belangrijk om de paramtere `all.x` op `TRUE` te zetten zodat alle data uit het dataframe behouden blijft. 

``` r 
df <- merge(df, alcoholscores, by = c("SubjectID", "TestNr"), all.x = TRUE)
```

### Test ID Aanmaken

Nadat deze data gemerged is, is er een dataframe ontstaan met alle testen die zijn afgenomen. Om later goed de testen te kunnen queryen met de data is het belangrijk dat iedere test een unieke ID heeft. Hiervoor hebben we niet voor een Auto Increment ID gekozen maar voor een leesbare ID. Deze is als volgt gestructureerd: 

```

Voorbeeld ID: 067021

De eerste 3 getallen staan voor de SubjectID
	> [067]
De volgende 2 getallen staan voor het test nummer (de hoeveelste test dit is van de subject)
	> [02]
Het laatste getal kan een 1 of een 0 zijn. 1 voor met simulatie bril, en 1 zonder simulatie bril. 
	> [1]
	
	
VOORBEELD: 
Subjet 67 is 2 keer terug komen voor een test, dan heeft deze subject dus 4 test records, namelijk:
- 067010	: test 1 zonder bril
- 067011	: test 1 met bril
- 067020	: test 2 zonder bril
- 067021	: test 1 met bril

```

Dit is gerealiseerd door het volgende stuk code: 

``` r
df <- cbind(tID = 0, df)
df$tID <- mapply(function(sID, tNr, goggels) {
                     paste(
                       str_pad(sID, 3, pad = "0"),
                       str_pad(tNr, 2, pad = "0"),
                       goggels,
                       sep = ""
                     )   
                  },
                  sID = df$SubjectID,
                  tNr = df$TestNr,
                  goggels = df$Goggles
)
```

### Filenaam Aanmaken

Nu zijn alle tests netjes opgeslagen in een dataframe. Bij iedere test hoort een apparte CSV file met de kinect data. De bestandsnamen van deze CSV Files zijn allemaal netjes gestructureerd, namelijk; 

```
Voorbeeld file: B1_norm_S161.csv

[testsoort][testNr]_[bril]_S[SubjectID].csv

Testsoort:	B = Balans
TestNr:		Hoeveelste test dit is van de subject
Bril:		Of de subjet een simulatiebril op had bij de test (bril | norm)
```

Om te bepalen welk bestand bij welk test hoort in het dataframe hebben we een stukje *reversed engineering* toegepast, dit is mogelijk omdat de bestandsnamen van de data zo goed gestructureerd zijn.

Van de data die we al hebben van iedere test hebben we een bestandsnaam 'gegenereerd'. 

``` r 
df$FileName <- mapply(function(testNr, glasses, subjectId) {
                        testNr <- paste("B", testNr, sep = "")
                        if (glasses == 1) {
                          glasses <- "bril"
                        } else {
                          glasses <- "norm"
                        }
                        subject <- paste("S", subjectId, sep = "")
                        
                        fn <- paste(testNr, glasses, subject, sep = "_")
                        return(paste(fn, "csv", sep = "."))
                      },
                      testNr = df$TestNr,
                      glasses = df$Goggles,
                      subjectId = df$SubjectID
)
```

Nu we alle bestandsnamen hebben willen we ook zeker weten dat deze bestanden ook daadwerkelijk bestaan. Daarom vergelijken we de bestandnamen met de bestanden die daadwerkelijk uit de folder komen. 

``` r 
fileNamesFolder <- as.data.frame(list.files("BalansKinectTests", pattern="*.csv", full.names = FALSE))
fileNamesDf <- as.data.frame(as.vector(df['FileName']))

names(fileNamesDf)[1] <- "name"
names(fileNamesFolder)[1] <- "name"

unknownFiles <- setdiff(fileNamesDf$name, fileNamesFolder$name)
```

Hieruit is gebleken dat er 5 bestanden niet overeenkomen. Of het bestand heeft geen record in het dataframe, of het dataframe heeft geen bestand. Hoe dan ook, dit is inrelevante data en wordt verwijderd uit het dataframe. 

``` r 
df <- df[!(df$FileName %in% unknownFiles), ]
```

Nu houden we 426 test records over.

### Kinect Data Toevoegen

Now this is where the magic happens. Alle subject data gaat nu gekoppeld worden aan de testdata van de kinect. 

Ieder kinect bestand ( = 1 test) bevat 119 variablen (frame time + alle coordinaten)

## Veld namen
Alle veldnamen van de data hebben we hernoemt om ze korten te maken. Dit is makkelijker leesbaar als je werkt in code en neem minder beslag als de data eenmaal in MongoDB is gezet. Hieronder een voorbeeld regel code voor 1 veld naam dat een nieuwe naam heeft gekregen.

``` r 
names(df)[names(df)=="SubjectID"] <- "sID"
```

Hieronder staat een overzichtje van de oude namen en nieuwe namen.
Oude naam" | "Nieuwe naam
--- | ---
"SubjectID" | "sID"  
"TestNr" | "Nr"  
"CarSickness" | "CarSick"  
"BalansProblem" | "BalansP"  
"Injury" | "Injur"  
"Opmerkingen" | "Note"  
"datetime" | "DateTime"  
"AlcoholProm" | "AlcP"  
"Time" | "FT"  

### Coördinaten namen.
Alle coordinaten hebben ook een nieuwe kortere naam gekregen. In de data heeft iedere coordinaat uiteindelijk 4 velden. Dat zijn `<coordinaatnaam><dimensie>`.
Bijvoorbeeld voor "SpineBase" geldt dat `"SpineBase_x"`, `"SpineBase_y"`, `"SpineBase_z"` en `"SpineBase_t"` veranderd zijn naar `"SBx"`, `"SBy"`, `"SBz"` en `"SBt"` repectievelijk. Hieronder staat een overzicht van oudenamen en nieuwe namen.  

Oude naam | Nieuwe naam
--- | ---
"SpineBase" | "SB"  
"SpineMid" | "SM"  
"Neck" | "N"  
"ShoulderLeft" | "SL"  
"ElbowLeft" | "EL"  
"WristLeft" | "WL"  
"HandLeft" | "HL"  
"ShoulderRight" | "SR"  
"ElbowRight" | "ER"  
"WristRight" | "WR"  
"HandRight" | "HR"  
"HipLeft" | "HiL"  
"KneeLeft" | "KN"  
"AnkleLeft" | "AL"  
"FootLeft" | "FL"  
"HipRight" | "HiR"  
"KneeRight" | "KR"  
"AnkleRight" | "AR"  
"FootRight" | "FR"  
"SpineShoulder" | "SS"  
"HandTipLeft" | "HTL"  
"ThumbLeft" | "TL"  
"HandTipRight" | "HTR"  
"ThumbRight" | "TR"  
















