# Research plots in R
Na het opschonen en opslaan van de data zijn is de verwerkt in de Mongo database. 
De volgende stap is de data onderzoeken. In dit onderzoek proberen we te achterhalen welke punten van het skelet het belangrijkst zijn, zodat niet alle 24 punten berekend hoeven te worden in het definitieve algortime.


## Bewegings visualisatie
Met behulp van scatterplots is het mogelijk om heel makkelijk de coördinaten te visualiseren en met elkaar te vergelijken. Deze vergelijk kan worden gedaan op twee dezelfde lichaamspunten uit verschillende tests. De scatterplot laat de beweging van een lichaamspunt zien. Op deze manier vergelijken we tests van dezelfde persoon waarbij bij de ene test niet is beïnvloed en de andere test beïnvloed door een alcoholsimulatie bril. Deze manier van visualiseren gebeurt maar in 2 dimensies, terwijl de kinect data 3 demensionaal is.